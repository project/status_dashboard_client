# Status Dashboard Client

The Status Dashboard Client module is developed as a supporting module
for the base module Status Dashboard. The Status Dashboard Client module
works on the client site and send information about available updates for
Drupal core version and Drupal modules to base module Status Dashboard.

For the module to work, you need to install on the base site the module
[status_dashboard](www.drupal.org/project/status_dashboard)

For a full description of the module, visit the
[project page](https://www.drupal.org/project/status_dashboard_client).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/status_dashboard_client).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

On base site you have to install the base module.

- [Status Dashboard](https://www.drupal.org/project/status_dashboard)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Install the Status Dashboard Client module
2. Enable the module
3. Open configuration form on URL /admin/config/development/status-dashboard-
   client
4. Add data for the Secret field, which is specified in the main site for
   connecting to this site.
5. Install on base site the Status Dashboard module
6. Enable the module
7. Open configuration form on URL /admin/config/development/status-dashboard
8. Add the data to the URL AND SECRET fieldset which has the property multiple :
    - URL - the name of the client site whose updates you want to monitor.
    - Secret - secret data for connecting with client site.
9. If you want to receive notifications by mail, then you need to fill the EMAIL
   NOTIFICATIONS fieldset by clicking the Enable email notifications checkbox:
    - Email - Email address for sending notifications.
    - Period - Notification frequency which consists of three periods (Daily,
      Weekly, Monthly).
    - Notification types - Types of updates (Core updates, Security updates,
      Feature updates) to be sent to the specified email


## Maintainers

- Farid Muborakshoev - [farid.muborakshoev](https://www.drupal.org/u/faridmuborakshoev)
- Bram Driesen - [BramDriesen](https://www.drupal.org/u/bramdriesen)
- Ilya Shablovsky - [aylis](https://www.drupal.org/u/aylis)
- Alexey Sivets - [w.drupal](https://www.drupal.org/u/wdrupal)

Supporting organization:
- [Solvy](https://www.drupal.org/solvy)
- [Sopra Steria](https://www.drupal.org/sopra-steria)
