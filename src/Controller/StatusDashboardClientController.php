<?php

namespace Drupal\status_dashboard_client\Controller;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Render\RendererInterface;
use Drupal\system\SystemManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\update\UpdateManagerInterface;

/**
 * Returns responses for Status dashboard client routes.
 */
class StatusDashboardClientController extends ControllerBase {

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * The system manager service.
   *
   * @var \Drupal\system\SystemManager
   */
  protected $systemManager;

  /**
   * The injected renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a new StatusDashboardClientController.
   *
   * @param \Drupal\Core\Extension\ModuleExtensionList $extension_list_module
   *   The module extension list.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The injected renderer.
   * @param \Drupal\system\SystemManager $system_manager
   *   The system manager service.
   */
  public function __construct(ModuleExtensionList $extension_list_module, TimeInterface $time, RequestStack $request_stack, RendererInterface $renderer, SystemManager $system_manager) {
    $this->moduleExtensionList = $extension_list_module;
    $this->time = $time;
    $this->request = $request_stack->getCurrentRequest();
    $this->renderer = $renderer;
    $this->systemManager = $system_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('extension.list.module'),
      $container->get('datetime.time'),
      $container->get('request_stack'),
      $container->get('renderer'),
      $container->get('system.manager')
    );
  }

  /**
   * Check the request.
   */
  public function doCheck(Request $request) {
    $projects_data = [];
    if ($available = update_get_available(TRUE)) {
      $this->moduleHandler()->loadInclude('update', 'compare.inc');
      $projects_data = update_calculate_project_data($available);
    }

    $security_updates = [];
    $feature_updates = [];

    $modules = $this->moduleExtensionList->getList();
    $modules_info = [];
    foreach ($projects_data as $project_data) {
      if (empty($project_data['recommended'])) {
        continue;
      }
      if (array_key_exists('security updates', $project_data) && !empty($project_data['security updates'] && $project_data['status'] <= UpdateManagerInterface::NOT_SUPPORTED)) {
        $security_updates[$project_data['title']] = $project_data['recommended'];
      }
      else {
        if ($project_data['recommended'] !== $project_data['existing_version']) {
          $feature_updates[$project_data['title']] = $project_data['recommended'];
        }
      }
    }
    foreach ($modules as $module) {
      $modules_info[$module->info['name']] = $module->info['version'];
    }
    $system_manager = $this->systemManager;
    $error_counter = 0;
    foreach ($system_manager->listRequirements() as $line) {
      // Only errors (severity >W 1) will trigger by default.
      // Severity > 0 to trigger both errors or warnings.
      if (isset($line['severity']) && $line['severity'] > 1) {
        $error_counter++;
      }
    }
    $json_response = [
      'date' => $this->time->getCurrentTime(),
      'core' => \Drupal::VERSION,
      'modules' => $modules_info,
      'security_updates' => $security_updates,
      'feature_updates' => $feature_updates,
      'sitename' => $this->config('system.site')->get('name'),
      'url' => $this->request->getSchemeAndHttpHost(),
      'error_count' => $error_counter,
    ];

    $this->moduleHandler()->alter('status_dashboard_json_response', $json_response, $projects_data);
    return new JsonResponse($json_response, 200);
  }

}
